from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from etc.config import current_config


class Tasks:
    def __init__(self):
        if current_config == 'CI':
            self.driver = webdriver.Remote("http://selenium__standalone-chrome:4444/wd/hub", DesiredCapabilities.CHROME)
        elif current_config == 'developer':
            self.driver = webdriver.Chrome()
        self.driver.get("http://todomvc.com/examples/react/")

    def create_task(self, title):
        new = WebDriverWait(self.driver, 3). \
            until(EC.visibility_of_element_located((By.XPATH, '//input[@class="new-todo"]')))
        new.send_keys(title)
        new.send_keys(Keys.RETURN)

    @property
    def count_active_tasks(self):
        return int(self.driver.find_element_by_xpath('//span[@class="todo-count"]/strong').text)

    @property
    def count_tasks(self):
        return int(self.driver.find_element_by_xpath('/html/body/section/div/section/ul').text.count('\n')) + 1

    def mark_completed(self, title):
        for i in range(1, self.count_tasks + 1):
            if self.driver.find_element_by_xpath(f'/html/body/section/div/section/ul/li[{i}]/div/label').text == title:
                self.driver.find_element_by_xpath(f'//li[{i}]/div/input[@class="toggle"]').click()
                break

    def delete_task(self, title):
        for i in range(1, self.count_tasks + 1):
            if self.driver.find_element_by_xpath(f'/html/body/section/div/section/ul/li[{i}]/div/label').text == title:
                destroy = self.driver.find_element_by_xpath(f'/html/body/section/div/section/ul/li[{i}]/div/button')
                self.driver.execute_script("arguments[0].click();", destroy)
                break

    def reload(self):
        self.driver.refresh()

    def quit(self):
        self.driver.quit()
