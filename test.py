from page_objects import Tasks

tasks = ["New task 1", "New task 2", "New task 3", "New task 4", "New task 5"]


def test_add_tasks():
    page = Tasks()
    for task in tasks:
        page.create_task(task)
    assert page.count_active_tasks == 5
    page.quit()


def test_complete_tasks():
    page = Tasks()
    for task in tasks:
        page.create_task(task)
    page.mark_completed(tasks[1])
    page.mark_completed(tasks[2])
    assert page.count_active_tasks == 3
    assert page.count_tasks == 5
    page.quit()


def test_delete_tasks():
    page = Tasks()
    for task in tasks:
        page.create_task(task)
    page.mark_completed(tasks[1])
    page.mark_completed(tasks[2])
    page.delete_task(tasks[0])
    page.delete_task(tasks[2])
    page.delete_task(tasks[4])
    assert page.count_active_tasks == 1
    assert page.count_tasks == 2
    page.quit()


def test_save_state():
    page = Tasks()
    for task in tasks:
        page.create_task(task)
    page.mark_completed(tasks[1])
    page.mark_completed(tasks[2])
    page.delete_task(tasks[0])
    page.delete_task(tasks[2])
    page.delete_task(tasks[4])
    page.reload()
    assert page.count_active_tasks == 1
    assert page.count_tasks == 2
    page.quit()


if __name__ == '__main__':
    test_add_tasks()
    print('Тест добавления нескольких задачь прошел успешно.')
    test_complete_tasks()
    print('Тест перевода задачь в статус "Completed" прошел успешно.')
    test_delete_tasks()
    print('Тест удаления задачь прошел успешно.')
    test_save_state()
    print('Тест сохранения состояния задачь после перезагрузки страницы прошел успешно.')
    print('Все тесты прошли!')
